import { Component } from '@angular/core';
import { Jedi } from './Jedi';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  jedis = [];

  constructor() {
    this.jedis.push(new Jedi('Luke Skywalker', 1.9));
    this.jedis.push(new Jedi('Darth Vader', 2.3));
    this.jedis.push(new Jedi('Yoda', 4.9));
    this.jedis.push(new Jedi('Obi Wan Kenobi', 1.7));
    this.jedis.push(new Jedi('Rey', 1.7));
  }

  adicionar(nomeJedi: string) {
    this.jedis.push(new Jedi(nomeJedi));
  }
}
